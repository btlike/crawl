package main

import (
	"net/http"
	_ "net/http/pprof"

	"gitlab.com/btlike/crawl/spider"
	"gitlab.com/btlike/crawl/utils"
)

func main() {
	go func() {
		http.ListenAndServe("localhost:6060", nil)
	}()

	utils.Init()
	spider.Run()
}
