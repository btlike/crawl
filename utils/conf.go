package utils

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/olivere/elastic.v3"
)

//defile vars
var (
	Config config
	Log    *log.Logger
	ES     *elastic.Client
)

type config struct {
	Elastic string `json:"elastic"`
	Number  int64  `json:"number"`
	Port    int    `json:"port"`
	Bep9    bool   `json:"bep9"`
}

//Init utilsl
func Init() {
	initLog()
	initConfig()
	initElastic()
}

func initElastic() {
	if Config.Elastic != "" {
		client, err := elastic.NewClient(elastic.SetURL(Config.Elastic))
		exit(err)
		ES = client
		ES.CreateIndex("torrent").Do()
	}
}

func initConfig() {
	f, err := os.Open("config/crawl.conf")
	exit(err)
	b, err := ioutil.ReadAll(f)
	exit(err)
	err = json.Unmarshal(b, &Config)
	exit(err)
}

func initLog() {
	Log = log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile)
}

func exit(err error) {
	if err != nil {
		Log.Fatalln(err)
	}
}

func Pretty(v interface{}) {
	b, _ := json.MarshalIndent(v, " ", "  ")
	log.Println(string(b))
}
