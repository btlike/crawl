package spider

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"gitlab.com/btlike/crawl/utils"
	"gitlab.com/btlike/spider"
)

//const
const (
	HashChanSize  = 512
	UniqHashSize  = 20 * 1000
	BlackListSize = 10 * 1000
	StoreChanSize = 1000
)

//Manager spider
var manage manager

func (p *manager) run() {
	p.initChan()
	p.initUniqHash()
	p.initBlackList()
	p.wire = NewWire()
	go p.monitor()
}

func (p *manager) initChan() {
	p.out = make(chan spider.Infohash, HashChanSize)
	p.storeMap = make(map[string]chan string)
	for i := 0; i <= 15; i++ {
		p.storeMap[fmt.Sprintf("%X", i)] = make(chan string, StoreChanSize)
	}
}

func (p *manager) initBlackList() {
	p.blacklist.mutex.Lock()
	defer p.blacklist.mutex.Unlock()
	for k := range p.blacklist.blackList {
		delete(p.blacklist.blackList, k)
	}
	p.blacklist.blackList = nil
	p.blacklist.blackList = make(map[string]bool, BlackListSize)
}

func (p *blacklist) set(s string) {
	p.mutex.Lock()
	p.blackList[s] = true
	p.mutex.Unlock()
}

func (p *blacklist) get(s string) bool {
	p.mutex.Lock()
	b := p.blackList[s]
	p.mutex.Unlock()
	return b
}

func (p *manager) initUniqHash() {
	p.uniqInfohash.mutex.Lock()
	defer p.uniqInfohash.mutex.Unlock()
	for k := range p.uniqInfohash.uniqInfohash {
		delete(p.uniqInfohash.uniqInfohash, k)
	}
	p.uniqInfohash.uniqInfohash = nil
	p.uniqInfohash.uniqInfohash = make(map[string]int64, UniqHashSize)
}

func (p *manager) infoHashAdd(hash string, count int64) {
	p.uniqInfohash.mutex.Lock()
	defer p.uniqInfohash.mutex.Unlock()
	if _, ok := p.uniqInfohash.uniqInfohash[hash]; ok {
		//已存在,属于重复通知
		return
	}
	p.uniqInfohash.uniqInfohash[hash] = count
	return
}

type blacklist struct {
	blackList map[string]bool
	mutex     sync.Mutex
}

type uniqInfohash struct {
	uniqInfohash map[string]int64
	mutex        sync.Mutex
}

type manager struct {
	wire       *Wire
	storeCount int64
	storeMap   map[string]chan string
	out        chan spider.Infohash

	uniqInfohash uniqInfohash
	blacklist    blacklist
}

func (p *manager) monitor() {
	go spider.Monitor()

	//定时flush热度数据到es
	go func() {
		for {
			p.flushInfohash()
			time.Sleep(time.Minute * 10)
		}
	}()

	//更新blacklitst
	for {
		if len(manage.blacklist.blackList) >= BlackListSize {
			p.initBlackList()
		}
		time.Sleep(time.Minute * 10)
	}
}

func (p *manager) flushInfohash() {
	cur := make(map[string]int64, UniqHashSize)
	utils.Log.Println("begin flush to es")
	p.uniqInfohash.mutex.Lock()
	for k, v := range p.uniqInfohash.uniqInfohash {
		cur[k] = v
	}
	p.uniqInfohash.mutex.Unlock()
	p.initUniqHash()

	cur2 := make(map[string]int64, UniqHashSize)
	for k, v := range cur {
		keys := strings.Split(k, ":")
		if len(keys) == 3 {
			raw := cur2[keys[2]]
			cur2[keys[2]] = raw + v
		}
	}
	increaseResourceHeat(cur2)
	utils.Log.Println("finish flush to es", len(cur2))
}
