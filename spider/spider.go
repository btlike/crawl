package spider

import (
	"encoding/hex"
	"fmt"

	"gitlab.com/btlike/crawl/utils"
	"gitlab.com/btlike/spider"
	"gopkg.in/olivere/elastic.v3"
)

//Run the spider
func Run() {
	manage.run()

	idList := spider.GenerateIDList(utils.Config.Number)
	for k, id := range idList {
		go func(port int, id spider.ID) {
			address := fmt.Sprintf(":%v", utils.Config.Port+port)
			spider.RunDhtNode(&id, manage.out, address)
		}(k, id)
	}

	go store()

	for result := range manage.out {
		if len(result.Infohash) == 40 &&
			result.IsAnnouncePeer {
			key := fmt.Sprintf("%s:%v:%s", result.IP.String(), result.Port, result.Infohash)
			manage.infoHashAdd(key, 1)
			go getMetadata(result)
		}
	}
}

func increaseResourceHeat(heat map[string]int64) {
	if len(heat) == 0 {
		return
	}
	reqs := []elastic.BulkableRequest{}
	for k, v := range heat {
		script := elastic.NewScript(fmt.Sprintf("ctx._source.Heat += %v", v))
		req := elastic.NewBulkUpdateRequest().Index("torrent").Type("infohash").Id(k).Script(script)
		reqs = append(reqs, req)
	}
	_, err := elastic.NewBulkService(utils.ES).Add(reqs...).Refresh(false).Do()
	if err != nil {
		utils.Log.Println(err)
		return
	}
}

func getMetadata(result spider.Infohash) (err error) {
	infohashByte, _ := hex.DecodeString(result.Infohash)
	manage.wire.fetchMetadata(Request{
		Port:     result.Port,
		IP:       result.IP.String(),
		InfoHash: infohashByte})
	return
}

func store() {
	for resp := range manage.wire.Response() {
		if len(resp.MetadataInfo) > 0 {
			metadata, err := Decode(resp.MetadataInfo)
			if err != nil {
				fmt.Println(err)
				continue
			}
			storeTorrent(metadata, resp.InfoHash)
		}
	}
}
